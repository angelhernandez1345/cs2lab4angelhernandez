//package edu.westga.cs1302.casino.game;
//
//import java.util.ArrayList;
//
//import edu.westga.cs1302.casino.model.Card;
//import edu.westga.cs1302.casino.model.Deck;
//import edu.westga.cs1302.casino.model.HumanPlayer;
//import edu.westga.cs1302.casino.model.Player;
//import edu.westga.cs1302.casino.resources.GameConstants;
//import edu.westga.cs1302.casino.resources.UI;
//
///**
// * The class Blackjack.
// * 
// * @author CS1302
// * @version Fall 2021
// */
//public class Blackjack {
//
//	public static final int DEALER_THRESHOLD = 17;
//
//	private Deck deck;
//	private Player dealer;
//	private HumanPlayer humanPlayer;
//
//	private int pot;
//
//	private int dealerWins;
//	private int humanWins;
//	private int ties;
//
//	private String message;
//
//	/**
//	 * Instantiates and starts a new game of Blackjack.
//	 * 
//	 * @precondition none
//	 * @postcondition a new game is started
//	 */
//	public Blackjack() {
//		this.dealer = new Player();
//		this.humanPlayer = new HumanPlayer(GameConstants.HUMAN_PLAYER_MONEY);
//		this.dealerWins = 0;
//		this.humanWins = 0;
//		this.ties = 0;
//		this.startNewRound();
//	}
//
//	/**
//	 * Starts a new round of BlackJack.
//	 * 
//	 * @precondition none
//	 * @postcondition ready to play
//	 */
//	public void startNewRound() {
//		this.message = UI.BLACKJACK;
//		this.pot = 0;
//		this.dealer.emptyHand();
//		this.humanPlayer.emptyHand();
//		this.deck = new Deck();
//		this.deck.shuffle();
//	}
//
//	/**
//	 * Deals the initial hands.
//	 * 
//	 * @precondition deck.size() == 52
//	 * @postcondition players get two cards each, deck.size() == deck.size()@prev -
//	 *                4
//	 */
//	public void dealHands() {
//		// TODO III-1
//	}
//
//	/**
//	 * Gets the message of this game.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the message
//	 */
//	public String getMessage() {
//		return this.message;
//	}
//
//	/**
//	 * The human player bets the specified bet amount and the pot is set to twice
//	 * the bet amount.
//	 * 
//	 * @precondition none
//	 * @postcondition getPot() == 2 * bet && humanPlayer.getMoney() ==
//	 *                humanPlayer.getMoney()@prev - bet
//	 * @param bet the bet from the player
//	 */
//	public void setPot(int bet) {
//		// TODO III-2
//	}
//
//	/**
//	 * Gets the dealer of this game.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the dealer
//	 */
//	public Player getDealer() {
//		return this.dealer;
//	}
//
//	/**
//	 * Gets the player of this game.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the player
//	 */
//	public HumanPlayer getHumanPlayer() {
//		return this.humanPlayer;
//	}
//
//	/**
//	 * Hit: player asks for an additional card from the deck.
//	 * 
//	 * @precondition none
//	 * @postcondition one card was dealt from the deck and added to the player's
//	 *                hand; if the player busts, the dealer wins the round and the
//	 *                message of the game is "Bust - you lose."
//	 * @return false if player busted, true otherwise
//	 */
//	public boolean hit() {
//		this.dealCardToHumanPlayer();
//		if (this.getHumanHandScore() > 21) {
//			this.dealerWinsRound("Bust - you lose.");
//			return false;
//		} else {
//			return true;
//		}
//	}
//
//	/**
//	 * Deals a card to the human player, form the top of the deck.
//	 * 
//	 * @precondition none
//	 * @postcondition top card from deck goes to the human player
//	 */
//	public void dealCardToHumanPlayer() {
//		Card card = this.deck.draw();
//		this.humanPlayer.addCard(card);
//	}
//
//	/**
//	 * Deals a card to the human player, form the top of the deck.
//	 * 
//	 * @precondition none
//	 * @postcondition top card from deck goes to the dealer
//	 */
//	public void dealCardToDealer() {
//		Card card = this.deck.draw();
//		this.dealer.addCard(card);
//	}
//
//	/**
//	 * Gets the pot of this game.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the pot
//	 */
//	public int getPot() {
//		return this.pot;
//	}
//
//	/**
//	 * Returns the human player's hand of cards.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the human player's cards
//	 */
//	public ArrayList<Card> getPlayerHand() {
//		return this.humanPlayer.getHand();
//	}
//
//	/**
//	 * Returns the dealer's hand of cards.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the dealer's cards
//	 */
//	public ArrayList<Card> getDealerHand() {
//		return this.dealer.getHand();
//	}
//
//	/**
//	 * Returns the statistics of this game.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the stats of the game as a string, formatted as the following
//	 *         example: You: 7\nDealer: 6\nTies: 4
//	 */
//	public String getStats() {
//		return "You: " + this.humanWins + System.lineSeparator() + "Dealer: " + this.dealerWins + System.lineSeparator()
//				+ "Ties: " + this.ties;
//	}
//
//	/**
//	 * Gets the player's score.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the score of the hand
//	 */
//	public int getHumanHandScore() {
//		return this.humanPlayer.getBlackjackScore();
//	}
//
//	/**
//	 * Gets the dealer's score.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the score of the hand
//	 */
//	public int getDealerHandScore() {
//		return this.dealer.getBlackjackScore();
//	}
//
//	/**
//	 * Gets the dealer's wins.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the dealerWins
//	 */
//	public int getDealerWins() {
//		return this.dealerWins;
//	}
//
//	/**
//	 * Gets the human player's wins.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the playerWins
//	 */
//	public int getHumanWins() {
//		return this.humanWins;
//	}
//
//	/**
//	 * Gets the number of ties of this.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * @return the ties
//	 */
//	public int getTies() {
//		return this.ties;
//	}
//
//	/**
//	 * The player has finished their turn and not busted, so now they stand. The
//	 * dealer plays their turn and the game ends.
//	 * 
//	 * @precondition the player stands
//	 * @postcondition the game reached a resolution
//	 */
//	public void playerStands() {
//		// TODO III.3.
//	}
//
//	private void dealerWinsRound(String message) {
//		if (this.getHumanPlayer().getMoney() == 0) {
//			this.message += UI.GAME_OVER;
//		} else {
//			this.message = message;
//			this.dealerWins++;
//		}
//	}
//}
