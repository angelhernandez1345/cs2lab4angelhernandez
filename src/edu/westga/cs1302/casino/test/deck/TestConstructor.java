package edu.westga.cs1302.casino.test.deck;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.casino.model.Deck;
import edu.westga.cs1302.casino.resources.GameConstants;

/**
 * Tests the correct functionality of Deck.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class TestConstructor {

	@Test
	public void testValidCreation() {
		Deck deck = new Deck();
		assertEquals(deck.getTopIndex(), GameConstants.DECK_SIZE);
	}
}
