	package edu.westga.cs1302.casino.test.deck;

	import static org.junit.jupiter.api.Assertions.assertEquals;

	import org.junit.jupiter.api.Test;

	import edu.westga.cs1302.casino.model.Card;
	import edu.westga.cs1302.casino.model.Deck;
	import edu.westga.cs1302.casino.model.Player;
	import edu.westga.cs1302.casino.model.Rank;
	import edu.westga.cs1302.casino.model.Suit;

	class TestAddingHand {

		@Test
		void test() {
			Player one = new Player();
			Card first = new Card(Rank.EIGHT, Suit.CLUBS);
			Card second = new Card(Rank.ACE, Suit.CLUBS);
			Card third = new Card(Rank.TWO, Suit.CLUBS);
			Card fourth = new Card(Rank.EIGHT, Suit.CLUBS);
			one.addCard(first);
			one.addCard(second);
			one.addCard(third);
			
			int result = one.getNumCardsInHand();
			int score = one.getBlackjackScore();
			
			assertEquals(3, result);
			assertEquals(21, score);
		}

	}
