package edu.westga.cs1302.casino.model;

/**
 * The enum Suit.
 * 
 * @author Angel Hernandez
 * @version 9/22/2021
 *
 */
public enum Suit {	
	DIAMONDS, CLUBS, HEARTS, SPADES
}
