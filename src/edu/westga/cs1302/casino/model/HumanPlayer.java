package edu.westga.cs1302.casino.model;

import edu.westga.cs1302.casino.resources.ExceptionMessages;

/**
 * The HumanPlayer class.
 * 
 * @author Angel Hernandez
 * @version 9/25/2021
 *
 */
public class HumanPlayer extends Player {
	private int money;
	
	/**
	 * Creates a HumanPlayer with a specified amount of money.
	 * 
	 * @precondition money > 0 
	 * @postcondition getMoney() == money
	 * @param money the money the HumanPlayer has
	 */
	public HumanPlayer(int money) {
		if (money < 0) {
			throw new IllegalArgumentException(ExceptionMessages.CANNOT_ENTER_CASINO);
		}
		this.money = money;
	}
	
	/**
	 * Returns the amount of money the HumanPlayer has.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the amount of money
	 */
	public int getMoney() {
		return this.money;
	}
	
	/**
	 * Allows the player to place a bet in the specified amount.
	 * 
	 * @precondition amount > 0 && amount <= getMoney()
	 * @postcondition getMoney() == getMoney()@prev - amount
	 * @param amount the amount to bet
	 */
	public void bet(int amount) {
		if (amount <= 0 | amount > this.money) {
			throw new IllegalArgumentException(ExceptionMessages.INVALID_AMOUNT);
		}
		
		this.money -= amount;
	}
	
	/**
	 * Add the specified amount to the player's money amount.
	 * 
	 * @precondition amount > 0
	 * @postcondition getMoney() == getMoney()@prev + amount
	 * @param amount the amount to add to the player's money amount
	 */
	public void receive(int amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException(ExceptionMessages.INVALID_AMOUNT);
		}
		this.money += amount;
	}
}
