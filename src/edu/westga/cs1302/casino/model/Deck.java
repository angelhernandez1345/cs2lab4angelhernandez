package edu.westga.cs1302.casino.model;

import java.util.Random;

import edu.westga.cs1302.casino.resources.ExceptionMessages;
import edu.westga.cs1302.casino.resources.GameConstants;

/**
 * This class represents a deck of playing cards.
 * 
 * @author CS 1302
 * @version Fall 2021
 */
public class Deck {

	private Card[] cards;
	private int topIndex;

	/**
	 * Constructs a new deck of cards.
	 * 
	 * @precondition none
	 * @postcondition the deck is instantiated && size() == 52
	 */
	public Deck() {
		Suit[] suits = { Suit.SPADES, Suit.HEARTS, Suit.DIAMONDS, Suit.CLUBS };
		this.cards = new Card[GameConstants.DECK_SIZE];
		this.topIndex = 0;
		for (Rank rank : Rank.values()) {
			for (Suit suit : suits) {
				if (rank.getValue() != 11) {
					Card card = new Card(rank, suit);
					this.cards[this.topIndex++] = card;
				}
			}
		}
	}

	/**
	 * Returns the top index of this deck.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the top index of this deck
	 */
	public int getTopIndex() {
		return this.topIndex;
	}

	/**
	 * Returns the card at the top of the deck.
	 * 
	 * @precondition size() > 0
	 * @postcondition getCard(getTopIndex()) == null && getTopIndex() ==
	 *                getTopIndex()@prev - 1
	 * @return card at top of deck
	 * @throws IAE with message EMPTY_DECK
	 */
	public Card draw() {
		if (this.topIndex == 0) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_DECK);
		}
		Card card = this.cards[--this.topIndex];
		this.cards[this.topIndex] = null;
		return card;
	}

	/**
	 * Shuffles the deck of cards using Knuth's algorithm.
	 * 
	 * @precondition none
	 * @postcondition the deck is shuffled 
	 */
	public void shuffle() {
		Random random = new Random();
		for (int index = 0; index < this.cards.length - 1; index++) {
			int randomIndex = random.nextInt(index + 1);
			Card card = this.cards[randomIndex];
			this.cards[randomIndex] = this.cards[index];
			this.cards[index] = card;
		}
	}
}
