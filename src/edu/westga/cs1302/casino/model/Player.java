package edu.westga.cs1302.casino.model;

import java.util.ArrayList;

import edu.westga.cs1302.casino.resources.ExceptionMessages;

/**
 * The Player class.
 * 
 * @author Angel Hernandez
 * @version 9/25/2021
 *
 */
public class Player {
	private ArrayList<Card> hand;
	
	/**
	 * Creates a player. 
	 * 
	 * @precondition none
	 * @postcondition getHand() == 0
	 * 
	 */
	public Player() {
		this.hand = new ArrayList<Card>();
	}

	/**
	 * Returns the current cards of this hand.
	 * 
	 * @precondition none
	 * @postcondition none 
	 * @return the hand
	 */
	public ArrayList<Card> getHand() {
		return this.hand;
	}
	
	/**
	 * Adds the card to the hand
	 * 
	 * @precondition card != null
	 * @postcondition getNumCardsInHand() == getNumCardsInHand()@prev +1
	 * @param card the card to be added
	 */
	public void addCard(Card card) {
		if (card == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_CARD);
		}
		this.hand.add(card);
	}
	
	/**
	 * Empty their hand of cards.
	 * 
	 * @preconditon none
	 * @postcondition none
	 */
	public void emptyHand() {
		this.hand.clear();
	}
	
	/**
	 * Returns how many cards they hold.
	 * 
	 * @preconditon none
	 * @postcondition none
	 * @return the size of the hand
	 */
	public int getNumCardsInHand() {
		return this.hand.size();
	}
	
	/**
	 * Returns their hand score.
	 * 
	 * @preconditon none
	 * @postcondition none
	 * @return score hand score
	 */
	public int getBlackjackScore() {
		int score = 0;
		
		for (Card current : this.hand) {
			//score += current.getRank();
			
			if (current.isFaceCard()) {
				score += 10;
			} 
			
			//score += current.getRank();
			
			if (current.getRank() == 1) {
				if (score <= 10) {
					score += 11;
				} else {
					score += 1;	
				}
					
			}
			
			score += current.getRank();
		}
		
		return score;
	}
	
	/**
	 * Tells whether or not they have a Natural 21.
	 * 
	 * @precondition none
	 * @postconditon none
	 * @return true if a natural 21, false otherwise
	 */
	public boolean hasNatural21() {
	
		return false;
	}
}
