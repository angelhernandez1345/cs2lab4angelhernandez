package edu.westga.cs1302.casino.resources;

/**
 * This class contains the constants specific to this application.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class GameConstants {

	public static final int DECK_SIZE = 52;
	public static final int HUMAN_PLAYER_MONEY = 100;
	public static final int TWENTY_ONE = 21;

}
